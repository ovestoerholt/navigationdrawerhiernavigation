# README #

This sample Android application shows how to implement the following Android features:

* Navigation Drawer
* Hierarchical navigation when using navigation drawer
* Animation between fragments