package com.example.navdrawerhiernavigation;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

/**
 * A fragment representing the front of the card.
 */
public class CardFrontFragment extends Fragment {

    public CardFrontFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_card_front, container, false);

        // Enable fragment options menu
        setHasOptionsMenu(true);

        return rootView;
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        // Inflate the fragment action menu
        inflater.inflate(R.menu.actions_cardfrontfragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's flip-to-back menu
            // This operation adds another fragment to the fragment stack
            // The fragment itself is responsible for making the back button show
            case R.id.action_fliptoback:
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                // Animation. For animation definition check the project animation folder
                ft.setCustomAnimations(R.animator.card_flip_right_in,
                        R.animator.card_flip_right_out,
                        R.animator.card_flip_left_in,
                        R.animator.card_flip_left_out);

                ft.replace(R.id.container, new CardBackFragment());
                // Add fragment to back stack
                ft.addToBackStack(null);
                ft.commit();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MyActivity) activity).onSectionAttached(1);
    }

}
