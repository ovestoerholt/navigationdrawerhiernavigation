package com.example.navdrawerhiernavigation;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

/**
 * This class implements callbacks from 2 other classes:
 * NavigationDrawerFragment.NavigationDrawerCallbacks
 * - For determining what will happen when a navigation drawer is clicked
 * FragmentManager.OnBackStackChangedListener
 * - Listens to and controls the number of fragments in the stack (when hierarchical navigation)
 * - In this app used for determining when to show the navigation drawer button and when to show a back button.
 */
public class MyActivity extends Activity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, FragmentManager.OnBackStackChangedListener {

    private static String TAG = "MyActivity";

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    /**
     * Used to decide if the app is in the base navigation level
     */
    private boolean mIsElevatedLevel = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));


        // Enable the OnBackStackListener
        getFragmentManager().addOnBackStackChangedListener(this);

    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();

        switch(position) {
            case 0:
                ft.replace(R.id.container, new CardFrontFragment());
                break;
            default:
                ft.replace(R.id.container, PlaceholderFragment.newInstance(position + 1));

        }
        ft.commit();
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.my, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * This is the options menu for all global actions (like, in this case, the settings menu)
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * Implemented from interface FragmentManager.OnBackStackChangedListener
     * When there are more than one fragment in the stack the back button is shown ('<').
     * When at base level the navigation drawer button is shown.
     */
    @Override
    public void onBackStackChanged() {

        mIsElevatedLevel = (getFragmentManager().getBackStackEntryCount() > 0);

        if (mIsElevatedLevel) {
            NavigationDrawerFragment.showSubActionBar();
            Log.d(TAG, "Elevated level...");
        } else {
            NavigationDrawerFragment.showNavActionBar();
            Log.d(TAG, "Base level...");
        }
        // When the back stack changes, invalidate the options menu (action
        // bar).
        invalidateOptionsMenu();
    }



}
